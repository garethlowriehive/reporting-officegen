import { Router } from "express";
import { powerpointControlller } from "../controllers";

const router = Router({ mergeParams: true });

router.post("/create-powerpoint", powerpointControlller.createPowerpoint);

export default router;
