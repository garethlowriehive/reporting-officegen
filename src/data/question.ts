export const data = {
  question: {
    text:
      "How likely are you to recommend our organisation as a good place to work?",
    invites: 500,
    responses: 341,
    distribution: [
      {
        key: "0",
        count: 11
      },
      {
        key: "1",
        count: 15
      },
      {
        key: "2",
        count: 21
      },
      {
        key: "3",
        count: 22
      },
      {
        key: "4",
        count: 33
      },
      {
        key: "5",
        count: 50
      },
      {
        key: "6",
        count: 102
      },
      {
        key: "7",
        count: 27
      },
      {
        key: "8",
        count: 24
      },
      {
        key: "9",
        count: 27
      },
      {
        key: "10",
        count: 9
      }
    ]
  }
};
