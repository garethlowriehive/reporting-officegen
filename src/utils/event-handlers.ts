export const addEventHandlers = pptx => {
  pptx.on("finalize", written => {
    console.log(
      "Finished to create a PowerPoint file.\nTotal bytes created: " +
        written +
        "\n"
    );
  });

  pptx.on("error", err => {
    console.log(err);
  });
};
