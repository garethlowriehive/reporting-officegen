import * as path from "path";
import { data } from "../data/question";

export const SLIDE_WIDTH = 1280;
export const FAKE_SLIDE_WIDTH = 1620; // Con: For some reason the slide is 460px bigger than what it is set to.
export const SLIDE_HEIGHT = 720;
const SLIDE_GUTTER = 80;

const LOGO_WIDTH = 135;
const LOGO_HEIGHT = 54;

const addTitleToSlide = (slide, title: string) => {
  slide.addText(title, {
    x: SLIDE_GUTTER,
    y: SLIDE_GUTTER,
    cx: 450,
    cy: 120,
    font_size: 46,
    font_face: "Verdana"
  });

  return slide;
};

const addLogoToSlide = slide => {
  slide.addImage(path.resolve(__dirname, "../images/logo.png"), {
    x: FAKE_SLIDE_WIDTH - SLIDE_GUTTER - LOGO_WIDTH,
    y: SLIDE_GUTTER,
    cx: LOGO_WIDTH,
    cy: LOGO_HEIGHT
  });

  return slide;
};

const addResponseRateToSlide = (slide, responseRate: number) => {
  slide.addText("Response Rate", {
    x: 60,
    y: 200,
    cx: 150,
    cy: 20,
    bold: true,
    font_size: 14,
    font_face: "Verdana"
  });

  return slide;
};

const addChartToSlide = slide => {
  return slide;
};

const setSlideDefaults = (slide, slideName: string) => {
  slide.name = slideName; // Slide name
  slide.back = "ffffff"; // Background color
  slide.color = "000000"; // Font color

  addLogoToSlide(slide);
  addChartToSlide(slide);
  addResponseRateToSlide(slide, 1);

  return slide;
};

export const createSlideOne = pptx => {
  const slide = pptx.makeNewSlide();
  addTitleToSlide(slide, "Question");
  setSlideDefaults(slide, "Question");

  return slide;
};
