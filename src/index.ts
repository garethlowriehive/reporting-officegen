import routes from "./routes";
import * as express from "express";

// Init express server.
const app = express();

// Configure routes to use.
app.use(routes);

// HTTP Server setup
const httpServer = app.listen(8080, () => {
  console.log("Listening...");
  process.send && process.send("ready");
});

process.on("SIGINT", async () => {
  // Shut down after 10 seconds if connections could not close in time
  setTimeout(() => {
    process.exit(0);
  }, 10000);

  await httpServer.close();

  process.exit(0);
});
