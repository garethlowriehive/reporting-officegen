// Based on example file from
// https://github.com/Ziv-Barber/officegen/blob/master/examples/make_pptx.js
import * as officegen from "officegen";
import { Request, Response } from "express";
import { addEventHandlers } from "../utils/event-handlers";
import { createSlideOne, SLIDE_HEIGHT, SLIDE_WIDTH } from "../utils/slide-one";

export const createPowerpoint = async (req: Request, res: Response) => {
  // 1. Initiate the PowerPoint and add event handlers
  const pptx = officegen("pptx");

  pptx.setSlideSize(SLIDE_WIDTH, SLIDE_HEIGHT);

  addEventHandlers(pptx);

  // 2. Set the Document Title
  pptx.setDocTitle("The Great Hive Report Off");

  // 3. Create the Slide One
  createSlideOne(pptx);

  // 4. Generate the PowerPoint and send it back via the response Write Stream.
  pptx.generate(res);
};
